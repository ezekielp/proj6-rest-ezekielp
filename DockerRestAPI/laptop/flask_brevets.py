"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging
import json


###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient('db', 27017)
# client = MongoClient(host='0.0.0.0', port=8000)
db = client.tododb
###
# Pages
###


class listAllJson(Resource):
    def get(self):
        documents = db.tododb.find().sort([('location', 1)])
        response = []
        for document in documents:
            loc = str(document['location'])
            op = str(document['open'])
            cl = str(document['close'])
            response.append(loc + "km Open Time: " + op)
            response.append(loc + "km Close Time: " + cl)
        return {
            'Control_Points': response
        }


class listOpenJson(Resource):
    def get(self):
        documents = retrieve()
        response = []
        for document in documents:
            loc = str(document['location'])
            op = str(document['open'])
            response.append(loc + "km Open Time: " + op)
        return {
            'Control_Points': response
        }


class listCloseJson(Resource):
    def get(self):
        documents = retrieve()
        response = []
        for document in documents:
            loc = str(document['location'])
            cl = str(document['close'])
            response.append(loc + "km Close Time: " + cl)
        return {
            'Control_Points': response
        }


class listAllCsv(Resource):
    def get(self):
        documents = db.tododb.find().sort([('location', 1)])
        response = "Location,Open,Close;"
        for document in documents:
            loc = str(document['location'])
            op = str(document['open'])
            cl = str(document['close'])
            response += loc + "km,Open Time: " + op + ",Close Time: " + cl + ";"
        response = response.strip(";")
        return response


class listOpenCsv(Resource):
    def get(self):
        documents = retrieve()
        response = "Location,Open;"
        for document in documents:
            loc = str(document['location'])
            op = str(document['open'])
            response += loc + "km,Open Time: " + op + ";"
        response = response.strip(";")
        return response


class listCloseCsv(Resource):
    def get(self):
        documents = retrieve()
        response = "Location,Close;"
        for document in documents:
            loc = str(document['location'])
            cl = str(document['close'])
            response += loc + "km,Close Time: " + cl + ";"
        response = response.strip(";")
        return response


def retrieve():
    """Helper function to retrieve the desired database entries"""
    top = request.args.get('top', 0, type=int)
    if top != 0:
        documents = db.tododb.find().sort([('location', 1)]).limit(top)
    else:
        documents = db.tododb.find().sort([('location', 1)])
    return documents


# Create routes
# Another way, without decorators
api.add_resource(listAllJson, '/listAll')
api.add_resource(listAllJson, '/listAll/json', endpoint='json')
api.add_resource(listCloseJson, '/listCloseOnly')
api.add_resource(listCloseJson, '/listCloseOnly/json', endpoint='/listCloseOny/json')
api.add_resource(listOpenJson, '/listOpenOnly')
api.add_resource(listOpenJson, '/listOpenOnly/json', endpoint='/listOpenOnly/json')
api.add_resource(listAllCsv, '/listAll/csv', endpoint='csv')
api.add_resource(listOpenCsv, '/listOpenOnly/csv', endpoint='/listOpenOnly/csv')
api.add_resource(listCloseCsv, '/listCloseOnly/csv', endpoint='/listCloseOnly/csv')


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('distance', 1000, type=int)
    date = request.args.get('begin_date', 'You messed up', type=str)
    time = request.args.get('begin_time', 'You messed up', type=str)

    #app.logger.debug("dist={}".format(dist))
    #app.logger.debug("date={}".format(date))
    #app.logger.debug("time={}".format(time))
    #app.logger.debug("km={}".format(km))

    combined = date + ' ' + time
    datetime = arrow.get(combined, 'YYYY-MM-DD HH:mm')

    #app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, dist, datetime.isoformat())
    close_time = acp_times.close_time(km, dist, datetime.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/_display')
def _display():
    _items = db.tododb.find()
    items = [item for item in _items]
    db.tododb.drop()    # Clear out for the next time it gets displayed
    return render_template('display.html', items=items)


@app.route('/_submit')
def _submit():
    # app.logger.debug("Begin submit function")
    # print(request.args.to_dict())
    km = request.args.get('kilometers', 996, type=float)
    o = request.args.get('open_time', "You fucked up", type=str)
    close = request.args.get('close_time', "You fucked up", type=str)
    item_doc = {
        'location': km,
        'open': o,
        'close': close
    }
    db.tododb.insert_one(item_doc)
    # app.logger.debug("End submit function")
    return redirect(url_for("index"))  # Don't do anything to the page on submit (only update the database)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
