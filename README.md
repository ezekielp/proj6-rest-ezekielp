# Project 6: Brevet time calculator with Ajax, MongoDB, and REST APIs

Author: Zeke Petersen     ezekielp@uoregon.edu

CIS 322 Fall 2019

Credits to Michal Young for the initial version of this code.

## ACP Calculator

This ACP calculator is intended to mimic the functionality of the RUSA calculator, but with an AJAX implementation. It will generate open and close times for the specified control points according to the algorithm described on the rusa.org website.

## Notes on the ACP controle times

The primary calculations use the following table:


| Control distance (km) | Minimum speed (km/hr) | Maximum speed (km/hr) |
|-----------------------|-----------------------|-----------------------|
| 0-200                 | 15                    | 34                    |
| 200-400               | 15                    | 32                    |
| 400-600               | 15                    | 30                    |
| 600-1000              | 11.428                | 28                    |
| 1000-1300             | 13.333                | 26                    |


For each control point, open times are calculated using maximum speeds and close times are calculated using minimum speeds. The control distance is divided by the speed to get the time. For distances beyond 200 km, multimple speeds will be used in the calculation, each corresponding to the distance associated with it.

For example, a 350km control point close time will use 34km/hr for the first 200km and 32km/hr for the next 150km for a total time in hours of (200/34) + (150/32).

However, there are further rules and specifications that alter this algorithm slightly. They are as follows:

- The close time for the starting control point (0km) is always 1hr after the official start.

- In keeping with the French algorithm, for the first 60km, close times are based on a 20km minimum speed, plus 1hr (ex: A control point at 20km would be 20km/hr/20km + 1hr = 2hrs). After 60, it returns to the specified 15km minimum speed benchmark to calculate close times (without the extra hour). Calculation for the open times does not deviate from the table.

- Any control point may not be in excess of 120% the distance of the brevet distance (ex. a control point may not be >360km in a 300km brevet). If the control point is greater than 120% the brevet distance or less than 0, the application will default to the start time (for both open and close). Inputs may only be numbers.

- Miles are converted to the nearest kilometer before running the algorithm. Times are rounded to the nearest minute.

- The final close times for the 5 legal brevet distances are as follows (regardless of final control point location, as long as it is in the legal range):
	 200km -- 13h30
	 300km -- 20h00
	 400km -- 27h00
	 600km -- 40h00
	 1000km - 75h00
This means that a control point greater than or equal to the brevet distance and less than or equal to 120% the brevet distance will use the appropriate time as the close time.

- Daylight savings is not accounted for. That is, for days after "spring forward" and before "fall back", open and close times will have 1 hour added to the normal output (If the start time input is 1PM on May 9th, a control point at zero km will have an open time of 2PM and a close time of 3PM).

## Notes on Submit and Display buttons

Submit: The submit button will take the currently populated fields and store the control points in a MongoDB database. If clicked multiple times, it will add copies of the fields to the database. Submit will throw an error on the page when there are no control times to enter.

Display: The display button will redirect the user to a new page and display all database entries representing the control points currently stored. Display clears the database immediately after displaying, so going back to the previous page and immediately displaying again will show a blank page (with a header).

## Notes on the Consumer Program

The consumer program will have several options for displaying the contents of the brevet database. The user will be able to select the data format (JSON or CSV) and will be able to choose whether to display all times, open only, or close only in both formats.

Additionally, the user will be able to query the top "x" entries in ascending order of either the open or the close times. Default (that is, if no query is specified) will be to display all times.

These times will be consuming APIs exposed by the flask_brevets server:
-----------------------------------------------------------------------

These first 3 default APIs will use a JSON format
"http://<host:port>/listAll" should return all open and close times in the database
"http://<host:port>/listOpenOnly" should return open times only
"http://<host:port>/listCloseOnly" should return close times only

"http://<host:port>/listAll/csv" should return all open and close times in CSV format
"http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
"http://<host:port>/listCloseOnly/csv" should return close times only in CSV format
"http://<host:port>/listAll/json" should return all open and close times in JSON format
"http://<host:port>/listOpenOnly/json" should return open times only in JSON format
"http://<host:port>/listCloseOnly/json" should return close times only in JSON format

These 4 query examples illustrate how times will be filtered by user input.
"http://<host:port>/listOpenOnly/csv?top=3" should return first 3 open times only (in ascending order) in CSV format
"http://<host:port>/listOpenOnly/json?top=5" should return first 5 open times only (in ascending order) in JSON format
"http://<host:port>/listCloseOnly/csv?top=6" should return first 5 close times only (in ascending order) in CSV format
"http://<host:port>/listCloseOnly/json?top=4" should return first 4 close times only (in ascending order) in JSON format
